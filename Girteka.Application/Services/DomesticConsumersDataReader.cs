﻿using CsvHelper;
using Girteka.Application.Mappers;
using Girteka.Application.Models;
using Microsoft.Extensions.Logging;
using System.Globalization;

namespace Girteka.Application.Services
{
    public class DomesticConsumersDataReader : IDomesticConsumersDataReader
    {
        private readonly ILogger _logger;

        public DomesticConsumersDataReader(ILogger<DomesticConsumersDataReader> logger)
        {
            _logger = logger;
        }

        public List<DomesticConsumersHourlyDataModel> Read(IEnumerable<string> paths)
        {
            var records = new List<DomesticConsumersHourlyDataModel>();

            foreach (var path in paths)
            {
                _logger.LogInformation($"Reading {path}");

                using (var reader = new StreamReader(path))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    csv.Context.RegisterClassMap<DomesticConsumersDataReaderModelMap>();
                    records.AddRange(csv.GetRecords<DomesticConsumersDataReaderModel>()
                                        .Select(x => new DomesticConsumersHourlyDataModel
                                                         (x.Network,
                                                          x.ObtName,
                                                          decimal.TryParse(x.PPlus, out decimal val1) ? val1 : 0,
                                                          decimal.TryParse(x.PMinus, out decimal val2) ? val2 : 0))
                                        .ToList());
                }
            }

            return records;
        }
    }
}
