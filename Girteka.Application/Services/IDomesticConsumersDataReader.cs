﻿using Girteka.Application.Models;

namespace Girteka.Application.Services
{
    public interface IDomesticConsumersDataReader
    {
        List<DomesticConsumersHourlyDataModel> Read(IEnumerable<string> paths);
    }
}
