﻿namespace Girteka.Application.Options
{
    public class PublicElectricityDataEndPointOptions
    {
        public const string PublicElectricityDataEndPoints = "PublicElectricityDataEndPoints";

        public string AprilDataUrl { get; set; } = string.Empty;
        public string MayDataUrl { get; set; } = string.Empty;
    }
}
