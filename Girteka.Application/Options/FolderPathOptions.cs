﻿namespace Girteka.Application.Options
{
    public class FolderPathOptions
    {
        public const string FolderPaths = "FolderPaths";

        public string AprilDataFolderPath { get; set; } = string.Empty;
        public string MayDataFolderPath { get; set; } = string.Empty;
    }
}
