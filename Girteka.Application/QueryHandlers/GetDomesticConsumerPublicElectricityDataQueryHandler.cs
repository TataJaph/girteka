﻿using Girteka.Application.Common;
using Girteka.Application.Common.Interface;
using Girteka.Application.Models;
using Girteka.Application.Queries;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Girteka.Application.QueryHandlers
{
    public class GetDomesticConsumerPublicElectricityDataQueryHandler :
        IRequestHandler<GetDomesticConsumerPublicElectricityDataQuery, ExecutionResult<IEnumerable<DomesticConsumersHourlyDataModel>>>
    {
        private readonly IGirtekaDbContext _context;

        public GetDomesticConsumerPublicElectricityDataQueryHandler(IGirtekaDbContext context)
        {
            _context = context;
        }

        public async Task<ExecutionResult<IEnumerable<DomesticConsumersHourlyDataModel>>> Handle
            (GetDomesticConsumerPublicElectricityDataQuery request, CancellationToken cancellationToken)
        {
            var result = new ExecutionResult<IEnumerable<DomesticConsumersHourlyDataModel>>
            {
                Success = true,
                Data = await _context.DomesticConsumersHourlyData
                                     .Select(x => new DomesticConsumersHourlyDataModel(x.Network, x.Obtname, x.PPlus, x.PMinus))
                                     .ToListAsync()
            };

            return result;
        }
    }
}
