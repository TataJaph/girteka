﻿using Girteka.Application.Commands;
using Girteka.Application.Common;
using Girteka.Application.Common.Interface;
using Girteka.Application.Constants;
using Girteka.Application.Extensions;
using Girteka.Application.Options;
using Girteka.Application.Services;
using Girteka.Domain.Entities;
using Girteka.Electricity.Integration;
using MediatR;
using Microsoft.Extensions.Options;

namespace Girteka.Application.CommandHandlers
{
    public class StoreAggregatedDataCommandHandler : IRequestHandler<StoreAggregatedDataCommand, ExecutionResult<DomainOperationResult>>
    {
        private readonly IGirtekaDbContext _context;
        private readonly IPublicElectricityClient _publicElectricityClient;
        private readonly IDomesticConsumersDataReader _domesticConsumersDataReader;
        private readonly PublicElectricityDataEndPointOptions _endpointOptions;
        private readonly FolderPathOptions _folderPathOptions;

        public StoreAggregatedDataCommandHandler(
            IGirtekaDbContext context,
            IPublicElectricityClient publicElectricityClient,
            IDomesticConsumersDataReader domesticConsumersDataReader,
            IOptions<PublicElectricityDataEndPointOptions> endpointOptions,
            IOptions<FolderPathOptions> folderPathOptions)
        {
            _context = context;
            _publicElectricityClient = publicElectricityClient;
            _domesticConsumersDataReader = domesticConsumersDataReader;
            _endpointOptions = endpointOptions.Value;
            _folderPathOptions = folderPathOptions.Value;
        }

        public async Task<ExecutionResult<DomainOperationResult>> Handle(StoreAggregatedDataCommand request, CancellationToken cancellationToken)
        {
            var result = new ExecutionResult<DomainOperationResult>()
            {
                Success = true
            };

            var aprilDocumentFileName = _endpointOptions.AprilDataUrl.TakeStringAfterSlash();
            var mayDocumentFileName = _endpointOptions.MayDataUrl.TakeStringAfterSlash();

            await _publicElectricityClient.DownloadHourlyDataOfDomesticConsumers
                (_endpointOptions.AprilDataUrl, _folderPathOptions.AprilDataFolderPath, aprilDocumentFileName);

            await _publicElectricityClient.DownloadHourlyDataOfDomesticConsumers
                (_endpointOptions.MayDataUrl, _folderPathOptions.MayDataFolderPath, mayDocumentFileName);

            var consumerData = _domesticConsumersDataReader
                .Read(new List<string> { $"{_folderPathOptions.AprilDataFolderPath}{aprilDocumentFileName}",
                                         $"{_folderPathOptions.MayDataFolderPath}{mayDocumentFileName}"});

            var aggregatedData = consumerData
                                        .Where(x => x.Obtname == DomesticConsumerConstants.Butas)
                                        .GroupBy(x => x.Network)
                                        .Select(p => new DomesticConsumersHourlyData
                                                         (p.Key,
                                                          p.FirstOrDefault()?.Obtname,
                                                          p.Sum(x => x.PPlus),
                                                          p.Sum(x => x.PMinus)))
                                        .ToList();

            await _context.DomesticConsumersHourlyData.AddRangeAsync(aggregatedData);
            await _context.SaveChangesAsync(cancellationToken);

            return result;
        }
    }
}
