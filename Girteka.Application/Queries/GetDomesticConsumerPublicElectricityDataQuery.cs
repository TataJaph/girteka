﻿using Girteka.Application.Common;
using Girteka.Application.Models;
using MediatR;

namespace Girteka.Application.Queries
{
    public class GetDomesticConsumerPublicElectricityDataQuery : IRequest<ExecutionResult<IEnumerable<DomesticConsumersHourlyDataModel>>>
    {
    }
}
