﻿using CsvHelper.Configuration;
using Girteka.Application.Models;

namespace Girteka.Application.Mappers
{
    public class DomesticConsumersDataReaderModelMap : ClassMap<DomesticConsumersDataReaderModel>
    {
        public DomesticConsumersDataReaderModelMap()
        {
            Map(p => p.Network).Name("Network", "TINKLAS").Optional();
            Map(p => p.ObtName).Name("ObtName", "OBT_PAVADINIMAS").Optional();
            Map(p => p.ObjType).Name("ObjType", "OBJ_GV_TIPAS").Optional();
            Map(p => p.ObjNumber).Name("ObjNumber", "OBJ_NUMERIS").Optional();
            Map(p => p.PPlus).Name("PPlus", "P+").Optional();
            Map(p => p.PLT).Name("PLT", "PL_T").Optional();
            Map(p => p.PMinus).Name("PMinus", "P-").Optional();
        }
    }
}
