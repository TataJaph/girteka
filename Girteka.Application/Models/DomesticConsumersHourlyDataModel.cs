﻿namespace Girteka.Application.Models
{
    public class DomesticConsumersHourlyDataModel
    {
        public DomesticConsumersHourlyDataModel() { }

        public DomesticConsumersHourlyDataModel(string network, string? obtname, decimal pPlus, decimal pMinus)
        {
            Network = network;
            Obtname = obtname;
            PPlus = pPlus;
            PMinus = pMinus;
        }

        public string Network { get; }
        public string? Obtname { get; }
        public decimal PPlus { get; }
        public decimal PMinus { get; }
    }
}
