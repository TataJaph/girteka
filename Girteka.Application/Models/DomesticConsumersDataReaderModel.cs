﻿namespace Girteka.Application.Models
{
    public class DomesticConsumersDataReaderModel
    {
        public string Network { get; set; }
        public string ObtName { get; set; }
        public string ObjType { get; set; }
        public string ObjNumber { get; set; }
        public string PPlus { get; set; }
        public string PLT { get; set; }
        public string PMinus { get; set; }
    }
}
