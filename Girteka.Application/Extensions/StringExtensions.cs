﻿namespace Girteka.Application.Extensions
{
    public static class StringExtensions
    {
        public static string TakeStringAfterSlash(this string str)
        {
            return str.Split('/').Last();
        }
    }
}
