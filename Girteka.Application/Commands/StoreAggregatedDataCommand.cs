﻿using Girteka.Application.Common;
using MediatR;

namespace Girteka.Application.Commands
{
    public class StoreAggregatedDataCommand : IRequest<ExecutionResult<DomainOperationResult>>
    {
    }
}
