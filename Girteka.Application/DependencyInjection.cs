﻿using Girteka.Application.Common.Behaviours;
using Girteka.Application.Services;
using Girteka.Electricity.Integration;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using RestSharp;
using System.Reflection;

namespace Girteka.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(UnhandledExceptionBehaviour<,>));
            services.AddTransient<IPublicElectricityClient, PublicElectricityClient>();
            services.AddTransient<IDomesticConsumersDataReader, DomesticConsumersDataReader>();
            services.AddTransient<RestClient>();

            return services;
        }
    }
}
