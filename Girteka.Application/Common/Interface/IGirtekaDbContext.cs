﻿using Girteka.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Girteka.Application.Common.Interface
{
    public interface IGirtekaDbContext
    {
        DbSet<DomesticConsumersHourlyData> DomesticConsumersHourlyData { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}
