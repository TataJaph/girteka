﻿namespace Girteka.Application.Common
{
    public class ExecutionResult<T>
    {
        public bool Success { get; set; }
        public string? Error { get; set; }
        public IEnumerable<Error> ErrorDetails { get; set; } = new List<Error>();
        public T? Data { get; set; }
    }

    public class Error
    {
        public string? Message { get; set; }
        public string? Property { get; set; }
    }
}
