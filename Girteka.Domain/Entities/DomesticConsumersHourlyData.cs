﻿namespace Girteka.Domain.Entities
{
    public class DomesticConsumersHourlyData
    {
        public DomesticConsumersHourlyData() { }

        public DomesticConsumersHourlyData(string network, string? obtname, decimal pPlus, decimal pMinus)
        {
            Network = network;
            Obtname = obtname;
            PPlus = pPlus;
            PMinus = pMinus;
        }

        public DomesticConsumersHourlyData(int id, string network, string? obtname, decimal pPlus, decimal pMinus)
        {
            Id = id;
            Network = network;
            Obtname = obtname;
            PPlus = pPlus;
            PMinus = pMinus;
        }

        public int Id { get; }
        public string Network { get; }
        public string? Obtname { get; }
        public decimal PPlus { get; }
        public decimal PMinus { get; }
    }
}
