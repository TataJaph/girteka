﻿using Girteka.Electricity.Integration.Exceptions;
using Girteka.Services.Providers;
using Microsoft.Extensions.Logging;
using RestSharp;

namespace Girteka.Electricity.Integration
{
    public class PublicElectricityClient : IPublicElectricityClient
    {
        private readonly IRestClientProvider _restClient;
        private readonly IDirectoryProvider _directoryProvider;
        private readonly IFileProvider _fileProvider;
        private readonly ILogger _logger;

        public PublicElectricityClient(
            IRestClientProvider restClient,
            IDirectoryProvider directoryProvider,
            IFileProvider fileProvider,
            ILogger<PublicElectricityClient> logger)
        {
            _restClient = restClient;
            _directoryProvider = directoryProvider;
            _fileProvider = fileProvider;
            _logger = logger;
        }

        public async Task DownloadHourlyDataOfDomesticConsumers(string downloadUrl, string filePath, string fileName)
        {
            if (!_directoryProvider.Exists(filePath))
            {
                _directoryProvider.CreateDirectory(filePath);

                _logger.LogInformation($"Folder created at - {filePath}");
            }

            _logger.LogInformation($"Downloading data - {downloadUrl}");

            var fileBytes = _restClient.DownloadData(new RestRequest(downloadUrl, Method.Get));

            if (fileBytes == null || !fileBytes.Any())
                throw new FileDownloadException();

            await _fileProvider.WriteAllBytesAsync(Path.Combine(filePath, fileName), fileBytes);
        }
    }
}
