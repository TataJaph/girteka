﻿namespace Girteka.Electricity.Integration
{
    public interface IPublicElectricityClient
    {
        Task DownloadHourlyDataOfDomesticConsumers(string downloadUrl, string filePath, string fileName);
    }
}
