using Girteka.Application;
using Girteka.Application.Common.Interface;
using Girteka.Application.Options;
using Girteka.Infrastructure.Persistence;
using Girteka.Services;
using Girteka.Web.Constants;
using Girteka.Web.Filters;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers(options =>
                options.Filters.Add<ApiExceptionFilterAttribute>());

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddApplication();
builder.Services.InjectServices();

builder.Services.AddDbContext<GirtekaDbContext>(opt => opt.UseSqlServer
            (builder.Configuration.GetConnectionString(AppSettingConstants.DbConnection)));
builder.Services.AddScoped<IGirtekaDbContext>(x => x.GetService<GirtekaDbContext>());

builder.Services.Configure<PublicElectricityDataEndPointOptions>(
    builder.Configuration.GetSection(PublicElectricityDataEndPointOptions.PublicElectricityDataEndPoints));
builder.Services.Configure<FolderPathOptions>(
    builder.Configuration.GetSection(FolderPathOptions.FolderPaths));

var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var dbContext = scope.ServiceProvider.GetRequiredService<GirtekaDbContext>();
    dbContext.Database.Migrate();
}

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();