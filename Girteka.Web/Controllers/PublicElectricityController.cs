﻿using Girteka.Application.Commands;
using Girteka.Application.Common;
using Girteka.Application.Models;
using Girteka.Application.Queries;
using Microsoft.AspNetCore.Mvc;

namespace Girteka.Web.Controllers
{
    public class PublicElectricityController : ApiControllerBase
    {
        [HttpPost]
        public async Task<ActionResult<ExecutionResult<DomainOperationResult>>> StoreAggregatedData(StoreAggregatedDataCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpGet]
        public async Task<ExecutionResult<IEnumerable<DomesticConsumersHourlyDataModel>>> ReadAggregatedData()
        {
            return await Mediator.Send(new GetDomesticConsumerPublicElectricityDataQuery());
        }
    }
}
