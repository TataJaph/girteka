﻿using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Girteka.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class ApiControllerBase : ControllerBase
    {
        private ISender? _mediator;

        protected ISender? Mediator => _mediator ??= HttpContext.RequestServices.GetService<ISender>();
    }
}
