﻿using Girteka.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Girteka.Infrastructure.Persistence.Configurations
{
    public class DomesticConsumersHourlyDataConfiguration : IEntityTypeConfiguration<DomesticConsumersHourlyData>
    {
        public void Configure(EntityTypeBuilder<DomesticConsumersHourlyData> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Network)
                .IsRequired()
                .HasMaxLength(50);

            builder.Property(x => x.Obtname)
                .IsRequired()
                .HasMaxLength(15);

            builder.Property(x => x.PPlus)
                .IsRequired();

            builder.Property(x => x.PMinus)
                .IsRequired();
        }
    }
}
