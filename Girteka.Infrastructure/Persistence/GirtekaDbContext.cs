﻿using Girteka.Application.Common.Interface;
using Girteka.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

namespace Girteka.Infrastructure.Persistence
{
    public class GirtekaDbContext : DbContext, IGirtekaDbContext
    {
        public GirtekaDbContext(DbContextOptions<GirtekaDbContext> options)
            : base(options)
        {
        }

        public DbSet<DomesticConsumersHourlyData> DomesticConsumersHourlyData { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(modelBuilder);
        }
    }
}
