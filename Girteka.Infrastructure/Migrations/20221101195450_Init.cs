﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Girteka.Infrastructure.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DomesticConsumersHourlyData",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Network = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Obtname = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: false),
                    PPlus = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    PMinus = table.Column<decimal>(type: "decimal(18,2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DomesticConsumersHourlyData", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DomesticConsumersHourlyData");
        }
    }
}
