using FakeItEasy;
using Girteka.Application.CommandHandlers;
using Girteka.Application.Commands;
using Girteka.Application.Common.Interface;
using Girteka.Application.Options;
using Girteka.Application.Services;
using Girteka.Electricity.Integration;
using Girteka.Electricity.Integration.Exceptions;
using Microsoft.Extensions.Options;

namespace Girteka.UnitTests
{
    [TestClass]
    public class StoreAggregatedDataCommandHandlerTest
    {
        private IGirtekaDbContext _context;
        private IPublicElectricityClient _publicElectricityClient;
        private IDomesticConsumersDataReader _domesticConsumersDataReader;
        private IOptions<PublicElectricityDataEndPointOptions> _endpointOptions;
        private IOptions<FolderPathOptions> _folderPathOptions;
        private StoreAggregatedDataCommandHandler _storeAggregatedDataCommandHandler;

        [TestInitialize]
        public void Init()
        {
            _context = A.Fake<IGirtekaDbContext>();
            _publicElectricityClient = A.Fake<IPublicElectricityClient>();
            _domesticConsumersDataReader = A.Fake<IDomesticConsumersDataReader>();
            _endpointOptions = Options.Create(new PublicElectricityDataEndPointOptions());
            _folderPathOptions = Options.Create(new FolderPathOptions());

            _storeAggregatedDataCommandHandler = new StoreAggregatedDataCommandHandler
                (_context, _publicElectricityClient, _domesticConsumersDataReader, _endpointOptions, _folderPathOptions);
        }

        [TestMethod]
        public async Task When_Electricity_Data_Download_Throws_Error_Handler_Should_Fail()
        {
            A.CallTo(() => _publicElectricityClient.DownloadHourlyDataOfDomesticConsumers(A<string>.Ignored, A<string>.Ignored, A<string>.Ignored))
                .Throws(new FileDownloadException());

            var command = new StoreAggregatedDataCommand();

            await Assert.ThrowsExceptionAsync<FileDownloadException>(() => _storeAggregatedDataCommandHandler.Handle(command, CancellationToken.None));
        }

        [TestMethod]
        public async Task When_Handle_Is_Success_SaveChanges_Should_Be_Called()
        {
            var command = new StoreAggregatedDataCommand();

            await _storeAggregatedDataCommandHandler.Handle(command, CancellationToken.None);

            A.CallTo(() => _context.SaveChangesAsync(CancellationToken.None)).MustHaveHappened();
        }

        [TestMethod]
        public async Task When_Handle_Is_Success()
        {
            var command = new StoreAggregatedDataCommand();

            var result = await _storeAggregatedDataCommandHandler.Handle(command, CancellationToken.None);

            Assert.IsTrue(result.Success);
        }
    }
}