﻿using FakeItEasy;
using Girteka.Electricity.Integration;
using Girteka.Electricity.Integration.Exceptions;
using Girteka.Services.Providers;
using Microsoft.Extensions.Logging;
using RestSharp;

namespace Girteka.UnitTests
{
    [TestClass]
    public class PublicElectricityClientTest
    {
        private IDirectoryProvider _directoryProvider;
        private IFileProvider _fileProvider;
        private IRestClientProvider _restClient;
        private IPublicElectricityClient _publicElectricityClient;
        private ILogger<PublicElectricityClient> _logger;

        [TestInitialize]
        public void Init()
        {
            _directoryProvider = A.Fake<IDirectoryProvider>();
            _fileProvider = A.Fake<IFileProvider>();
            _restClient = A.Fake<IRestClientProvider>();
            _logger = A.Fake<ILogger<PublicElectricityClient>>();

            _publicElectricityClient = new PublicElectricityClient(_restClient, _directoryProvider, _fileProvider, _logger);
        }

        [TestMethod]
        public async Task When_Directory_Does_Not_Exist_Create_Directory_Should_Be_Called()
        {
            var filePath = "C:/Folder";

            A.CallTo(() => _directoryProvider.Exists(A<string>.Ignored)).Returns(false);

            A.CallTo(() => _restClient.DownloadData(A<RestRequest>.Ignored)).Returns(new byte[] { 1 });

            await _publicElectricityClient.DownloadHourlyDataOfDomesticConsumers(string.Empty, filePath, string.Empty);

            A.CallTo(() => _directoryProvider.CreateDirectory(filePath)).MustHaveHappened();
        }

        [TestMethod]
        public async Task When_Download_Data_Returns_Empty_Array_FileDownloadException_Should_Be_Thrown()
        {
            A.CallTo(() => _directoryProvider.Exists(A<string>.Ignored)).Returns(true);

            A.CallTo(() => _restClient.DownloadData(A<RestRequest>.Ignored)).Returns(new byte[] { });

            await Assert.ThrowsExceptionAsync<FileDownloadException>
                (() => _publicElectricityClient.DownloadHourlyDataOfDomesticConsumers(string.Empty, string.Empty, string.Empty));
        }

        [TestMethod]
        public async Task When_Download_Data_Returns_Null_FileDownloadException_Should_Be_Thrown()
        {
            A.CallTo(() => _directoryProvider.Exists(A<string>.Ignored)).Returns(true);

            A.CallTo(() => _restClient.DownloadData(A<RestRequest>.Ignored)).Returns(null);

            await Assert.ThrowsExceptionAsync<FileDownloadException>
                (() => _publicElectricityClient.DownloadHourlyDataOfDomesticConsumers(string.Empty, string.Empty, string.Empty));
        }

        [TestMethod]
        public async Task When_Download_Data_Returns_Data_WriteAllBytesAsync_Should_Be_Called()
        {
            var byteArray = new byte[] { 1 };
            var filePath = "C:/Folder";
            var fileName = "File";

            A.CallTo(() => _directoryProvider.Exists(A<string>.Ignored)).Returns(true);

            A.CallTo(() => _restClient.DownloadData(A<RestRequest>.Ignored)).Returns(byteArray);

            await _publicElectricityClient.DownloadHourlyDataOfDomesticConsumers(string.Empty, filePath, fileName);

            A.CallTo(() => _fileProvider.WriteAllBytesAsync(Path.Combine(filePath, fileName), byteArray)).MustHaveHappened();
        }

        [TestMethod]
        public async Task When_DownloadHourlyDataOfDomesticConsumers_Is_Success()
        {
            A.CallTo(() => _directoryProvider.Exists(A<string>.Ignored)).Returns(false);

            A.CallTo(() => _restClient.DownloadData(A<RestRequest>.Ignored)).Returns(new byte[] { 1 });
            try
            {
                await _publicElectricityClient.DownloadHourlyDataOfDomesticConsumers(string.Empty, string.Empty, string.Empty);
            }
            catch (Exception)
            {
                Assert.Fail();
            }
        }
    }
}
