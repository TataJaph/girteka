﻿using Girteka.Services.Providers;
using Microsoft.Extensions.DependencyInjection;

namespace Girteka.Services
{
    public static class DependencyInjection
    {
        public static IServiceCollection InjectServices(this IServiceCollection services)
        {
            services.AddTransient<IDirectoryProvider, DirectoryProvider>();
            services.AddTransient<IFileProvider, FileProvider>();
            services.AddTransient<IRestClientProvider, RestClientProvider>();

            return services;
        }
    }
}
