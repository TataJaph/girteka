﻿namespace Girteka.Services.Providers
{
    public class FileProvider : IFileProvider
    {
        public async Task WriteAllBytesAsync(string path, byte[] bytes)
        {
            await File.WriteAllBytesAsync(path, bytes);
        }
    }
}
