﻿using RestSharp;

namespace Girteka.Services.Providers
{
    public class RestClientProvider : IRestClientProvider
    {
        private readonly RestClient _client;
        public RestClientProvider(RestClient client)
        {
            _client = client;
        }

        public byte[]? DownloadData(RestRequest request)
        {
            return _client.DownloadData(request);
        }
    }
}
