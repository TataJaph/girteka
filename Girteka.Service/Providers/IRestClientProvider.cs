﻿using RestSharp;

namespace Girteka.Services.Providers
{
    public interface IRestClientProvider
    {
        byte[]? DownloadData(RestRequest request);
    }
}
