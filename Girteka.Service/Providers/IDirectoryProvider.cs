﻿namespace Girteka.Services.Providers
{
    public interface IDirectoryProvider
    {
        bool Exists(string filePath);

        DirectoryInfo CreateDirectory(string filePath);
    }
}
