﻿namespace Girteka.Services.Providers
{
    public interface IFileProvider
    {
        Task WriteAllBytesAsync(string path, byte[] bytes);
    }
}
