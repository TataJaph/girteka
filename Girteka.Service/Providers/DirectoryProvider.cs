﻿namespace Girteka.Services.Providers
{
    public class DirectoryProvider : IDirectoryProvider
    {
        public DirectoryInfo CreateDirectory(string filePath)
        {
            return Directory.CreateDirectory(filePath);
        }

        public bool Exists(string filePath)
        {
            return Directory.Exists(filePath);
        }
    }
}
